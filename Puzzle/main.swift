//
//  main.swift
//  Puzzle
//
//  Created by Hardik on 18/12/16.
//  Copyright © 2016 Hardik. All rights reserved.
//

import Foundation

private func singleInputFromrKeyboard() -> Int {
    
    let keyboard = FileHandle.standardInput
    let inputData = keyboard.availableData
    let strData = String(data: inputData, encoding: .utf8)
    
    if let inputString = strData?.trimmingCharacters(in: CharacterSet.newlines), let inputInt = Int(inputString) {
        return inputInt
    } else {
        print("Enter only Int value")
        return singleInputFromrKeyboard()
    }
}

private func findMultiplyValuesFor(multiplier:Int, between start: Int, to end: Int) {
    
    for i in (start...end).reversed() where i % multiplier == 0 {
        print("\(i) ")
    }
}

print("Enter numbers of attemps...")

let attemptCount = singleInputFromrKeyboard()

for _ in 1...attemptCount {
    
    print("Range From :")
    let startIndex = singleInputFromrKeyboard()
    
    print("Range upto :")
    let endIndex = singleInputFromrKeyboard()
    
    print("Enter multiplier...")
    let multiplier = singleInputFromrKeyboard()
    
    findMultiplyValuesFor(multiplier: multiplier, between: startIndex, to: endIndex)
}



